# Changelog
This changelog is inspired by [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Release 0.1 [2022-01-10]
### Added
- coverage, makefile, setup.cfg

### Changed
- Using setup.cfg instead of setup.py

## Release 0.0b2.post2 [2020-08-21]
### Fixed
- Missing module declaration within setup.pyChanged

## Release 0.0b2.post1 [2020-08-20]
### Changed
- Changed headings and removed redundant 'example' statement within readme for
 better readability.

## Release 0.0b2 [2020-08-20]
### Changed
- Typos within readme and module.
